<div class="footer mt-5">
    <div class="container">
        <h3 class="text-center">
            <a href="#" data-social="linkedin" class="text-primary" target="_blank">
                <i class="fab fa-linkedin text-primary"></i>
            </a>
            <a href="#" data-social="github" class="text-dark" target="_blank">
                <i class="fab fa-github"></i>
            </a>
            <a href="#" data-social="google" class="text-danger" target="_blank">
                <i class="fab fa-google-plus-g text-danger"></i>
            </a>
        </h3>
    </div>
</div>
